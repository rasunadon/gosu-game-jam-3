require 'rubygems'
require 'gosu'

require_relative './lib/user_interface/game_window'

PROMPT = '> '

version = '0.4 dev'
$window = GameWindow.new(version)
$window.show
