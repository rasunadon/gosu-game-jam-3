require 'rake'

Gem::Specification.new do |spec|
  spec.name        = 'crane_construction'
  spec.version     = '0.3.1'
  spec.date        = '2022-08-29'
  spec.license     = 'CC-BY-SA-3.0'

  spec.summary = "A simulation construction game"
  spec.description = "Crane Construction is a simulation construction game. It
  was created during the third Gosu Game Jam."
  spec.files       = Rake::FileList["lib/lib/**/*",
                                    "lib/media/*.png",
                                    "lib/CHANGELOG.md",
                                    "lib/crane_construction.gemspec",
                                    "lib/crane_construction.rb",
                                    "lib/README.md"]

  spec.author      = 'Detros'
  spec.homepage    = 'https://rasunadon.itch.io/crane-construction'
  spec.email       = 'rasunadon@seznam.cz'
  spec.metadata = {
    "source_code_uri"   => "https://gitlab.com/rasunadon/crane-construction",
    "bug_tracker_uri"   => "https://gitlab.com/rasunadon/crane-construction/issues",
    "documentation_uri" => "https://gitlab.com/rasunadon/crane-construction/blob/master/README.md",
    "changelog_uri"     => "https://gitlab.com/rasunadon/crane-construction/blob/master/CHANGELOG.md",
    "homepage_uri"      => "https://rasunadon.itch.io/crane-construction",
  }

  spec.platform    = Gem::Platform::RUBY
  spec.add_runtime_dependency 'gosu', '~> 0.9'
  spec.add_development_dependency 'rake', '~> 10.0'
end
