# Crane Construciton
- game jam: https://itch.io/jam/gosu-game-jam-3
- gem: https://rubygems.org/gems/crane_construction
- repository: https://gitlab.com/rasunadon/crane-construction
- bug tracker: https://gitlab.com/rasunadon/crane-construction/issues
- licence: CC-BY-SA 3.0, Detros
- email: rasunadon@seznam.cz

_Crane Construction_ was created during the third Gosu Game Jam which run for
two weeks between 2022-08-14 and 2022-08-29. Your goal is to pile blocks as
you like as blueprints have not been finished yet. Game thus doesn't end.


# Controls
- hook movement: mouse
- start holding / drop: left mouse button
- rotate held block: R
- new block: N
- show debug info: Tab
- reset: Backspace
- quit: Esc


# Other documentation
- CHANGELOG: list of what has been added/changed/fixed/removed in given version
