# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Not released yet
### Added
- falling blocks collide with other blocks

### Changed
- bricks renamed to blocks

### Fixed
### Removed


## [v3.1](../../tags/v3.1) – 2022-08-29
### Added
- marketing media
- timer
- Rubygems gem and Itch.io project


## [v3](../../tags/v3) – 2022-08-29
### Added
- controls and summary in README
- bricks falling down when dropped in air

### Changed
- bricks rotate so that their top stays at the same height
- distinct key presses instead of continuous holding
- hook can hold also other bricks than the first one
- range of hook-attractedness limited

### Fixed
- held bricks can again be rotated


## [v2](../../tags/v2) – 2022-08-27
### Added
- one brick for a start
- brickyard can produce more bricks
- hook can hold and drop the first brick

### Changed
- movement curtailed to working area

### Fixed
- brickyard is drawing also itself


## [v1](../../tags/v1) – 2022-08-24
### Added
- close, reset and debug hotkeys
- basic scene
- hook follows mouse
