require_relative './../items/block'

class Blockyard
  attr_reader :blocks

  def initialize()
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png') # TODO

    reset!
  end

  # Load default setup
  def reset!
    @x = 0.8 * MAPX * TILESIZE
    @y = WINDOW_HEIGHT - GRASS_THICKNESS * TILESIZE
    @blocks = [] # all (collidable) blocks
  end

  def update(button)
    case button
      when Gosu::KB_N then
        produce!
    end

    # Update all blocks
    @blocks.each {|ii| ii.update(button)}
  end

  def draw
    # Draw self
    @image.draw_rot(@x, @y, ZITEMS, 0,
                center_x = 0.5, center_y = 0, # y: top
                scale_x = BLOCK_THICKNESS, scale_y = GRASS_THICKNESS / 2,
                color = Gosu::Color::WHITE)

    # Draw all blocks
    @blocks.each {|ii| ii.draw}

    if $debug
      info = Gosu::Image.from_text("[#{@blocks.count} block(s)]", LINE_HEIGHT)
      info.draw(@x, @y, ZTEXT)
    end
  end

  def produce!
    puts "producing" #TODO
    block_x = @x
    block_y = @y - BLOCK_LENGTH * TILESIZE # just above blockyard
    block_angle = 90

    new_block = Block.new(self, block_x, block_y, block_angle)
    @blocks.append(new_block)
  end
end
