TROLLEY_THICKNESS = 0.8 * CRANE_BOOM_THICKNESS # y-scaling of trolley
HOOK_THICKNESS = 0.15 # y-scaling of hook
HOOK_BORDER = TILESIZE / 2 # how much space is left between trolley and crane boom ends

HOOK_MIN_X = CRANE_BORDER + CRANE_MAST_X + CRANE_MAST_THICKNESS*TILESIZE + HOOK_BORDER
HOOK_MAX_X = CRANE_BOOM_LENGTH*TILESIZE - HOOK_BORDER
HOOK_MIN_Y = CRANE_BOOM_Y + (TROLLEY_THICKNESS + HOOK_THICKNESS) * TILESIZE
HOOK_MAX_Y = (MAPY - GRASS_THICKNESS) * TILESIZE

HOOK_MAGNET_X = TILESIZE / 3 # how much left/right of hook can a block be to be reachable
HOOK_MAGNET_Y = TILESIZE / 5 # how much below hook can a block be to be still reachable

class Hook
  def initialize(window, blockyard, crane)
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png')
    @window = window
    @blockyard = blockyard
    @crane = crane

    reset!
  end

  # Load default setup
  def reset!
    @x = CENTRE
    @y = CENTRE
    @angle = 0
    @held = nil
  end

  def update(button)
    case button
      when Gosu::MS_LEFT then
        if holding?
          drop!
        else
          hold!
        end
      when Gosu::KB_R then
        if holding?
          @held.rotate!
    end
    end

    # TODO directional movement instead of teleporting
    @x = @window.mouse_x.to_int
    @y = @window.mouse_y.to_int

    # Block movement (by blocks)
    # TODO

    # Curtail movement (by borders of working area)
    @x = [@x, HOOK_MIN_X].max
    @x = [@x, HOOK_MAX_X].min
    @y = [@y, HOOK_MIN_Y].max
    @y = [@y, HOOK_MAX_Y].min # MAX_Y does not need to care about status of
    # holding? as when TRUE the held block should have already blocked the move

    # Update held block
    if @held
      @held.x = @x
      @held.y = @y
    end
  end

  def holding?
    !@held.nil?
  end

  def drop!
    @held.start_falling!
    @held = nil
  end

  def hold!
    reachable_block = find_reachable_block
    if reachable_block
      @held = reachable_block
      @held.stop_falling! # to be sure
      @held.status = BLOCK_STATUS_BEING_HELD
    else
      puts "no blocks nearby"
    end
  end

  # Find nearest block under hook
  def find_reachable_block
    # TODO here
    reachables = @blockyard.blocks.select {
      |ii| ii.x.between?(@x - HOOK_MAGNET_X, @x + HOOK_MAGNET_X)
    } # block is a bit to the left or right of hook
    reachables = reachables.select {
      |ii| ii.y.between?(@y, @y + HOOK_MAGNET_Y)
    } # block is at the same height or bit lower than hook
    reachables[0] # if multiple in reach, pick the older
  end

  def draw
    @image.draw_rot(@x, CRANE_BOOM_Y, ZITEMS, 0,
                    center_x = 0.5, center_y = 0,
                    scale_x = 0.75, scale_y = TROLLEY_THICKNESS,
                    color = Gosu::Color::BLACK) # trolley
    @image.draw_rot(@x, CRANE_BOOM_Y, ZITEMS, 0,
                    center_x = 0.5, center_y = 0, # y: top
                    scale_x = 0.08, scale_y = (@y - CRANE_BOOM_Y) / TILESIZE,
                    color = Gosu::Color::BLACK) # cable
    @image.draw_rot(@x, @y, ZITEMS, 0,
                    center_x = 0.5, center_y = 1, # y: bottom
                    scale_x = 1, scale_y = HOOK_THICKNESS,
                    color = Gosu::Color::BLACK) # hook itself

    if $debug and not holding?
      coords = Gosu::Image.from_text("[#{@x}, #{@y}, #{@angle}°]", LINE_HEIGHT)
      coords.draw_rot(@x, @y, ZTEXT, @angle)
    end
  end
end
