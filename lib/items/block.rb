BLOCK_LENGTH = 3 # x/y-scaling of block
BLOCK_THICKNESS = 1 # y/x-scaling of block

BLOCK_STATUS_STANDING = 0
BLOCK_STATUS_BEING_HELD = 1
BLOCK_STATUS_FALLING = 2

BLOCK_FALL_ACCELERATION = 9.8 # Earth gravity
BLOCK_FALL_PRECISION = 3 # rounding precision of speed of fall

class Block
  attr_accessor :x, :y, :status

  def initialize(blockyard, xx, yy, angle)
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png') # TODO
    @blockyard = blockyard

    @x = xx
    @y = yy
    @angle = angle
    start_falling!
  end

  def start_falling!
    @status = BLOCK_STATUS_FALLING
    @falling_speed = 0
    @time_of_start_of_fall = Gosu.milliseconds
  end

  def stop_falling!
    @status = BLOCK_STATUS_STANDING
    @falling_speed = nil
    @time_of_start_of_fall = nil
  end

  def update(button)
    # TODO HERE
    case button
    when Gosu::KB_C then
      if status == BLOCK_STATUS_BEING_HELD
        look_below
      end
    end

    # Other states should not be needed here
    # - BLOCK_STATUS_STANDING does not need any updates
    # - BLOCK_STATUS_BEING_HELD is managed from Hook
    if status == BLOCK_STATUS_FALLING
      # Apply falling speed to y-coords (if possible)
      seconds_falling = (Gosu.milliseconds - @time_of_start_of_fall) / 1000.0
      fall_accel = BLOCK_FALL_ACCELERATION * seconds_falling * seconds_falling
      @falling_speed = (@falling_speed + fall_accel).round(BLOCK_FALL_PRECISION)

      # Is there space to fall to? Or is there some ledge to stand on?
      ledges_below = @blockyard.blocks.select {|ii| am_over?(ii)}
      nearest_ledge = ledges_below.min_by {|ii| ii.top} # y: highest
      ledge_y = if nearest_ledge.nil? then GRASS_Y else nearest_ledge.y end # TODO just count grass as another block?

      fall_y = (@y + @falling_speed).round(1)
      on_ledge_y = ledge_y - y_scaling * TILESIZE

      if fall_y >= on_ledge_y # stop falling
        @y = on_ledge_y
        stop_falling!
      else # continue falling
        @y = fall_y
      end
    end
  end

  # DEBUG only
  def look_below
    puts left
    puts right
    puts top
    puts bottom

    blocks_below = @blockyard.blocks.select {|ii| am_over?(ii)}

    if blocks_below.empty?
      puts 'nothing below'
    else
      puts blocks_below[0].y
    end
  end

  def am_over?(other)
    (other.right > self.left) and
    (other.left < self.right) and
    (other.top > self.bottom) # y: bellow
  end

  def draw
    if horizontal?
      cx = 0.5
      cy = 0 # y: top
    else ## vertical
      cx = 0 # turned x: top
      cy = 0.5
    end

    @image.draw_rot(@x, @y, ZITEMS, @angle,
                    center_x = cx, center_y = cy,
                    scale_x = BLOCK_LENGTH, scale_y = BLOCK_THICKNESS,
                    color = Gosu::Color::GRAY)

    if $debug
      coords = Gosu::Image.from_text("[#{@x}, #{@y}, #{@angle}°, S#{@status}]", LINE_HEIGHT)
      coords.draw_rot(@x, @y, ZTEXT, @angle)
    end
  end

  def rotate!
    @angle = 90 - @angle
  end

  # From (lower) x-border
  def left
    @x - (x_scaling / 2.0) * TILESIZE
  end

  # To (bigger) x-border
  def right
    @x + (x_scaling / 2.0) * TILESIZE
  end

  # From (lower) y-border
  def top
    @y
  end

  # To (bigger) y-border
  def bottom
    @y + y_scaling * TILESIZE
  end

  # X-scaling with rotation applied
  def x_scaling
    if horizontal?
      BLOCK_LENGTH
    else # vertical
      BLOCK_THICKNESS
    end
  end

  # Y-scaling with rotation applied
  def y_scaling
    if horizontal?
      BLOCK_THICKNESS
    else # vertical
      BLOCK_LENGTH
    end
  end

  # Not rotated?
  def horizontal?
    @angle == 0
  end
end
