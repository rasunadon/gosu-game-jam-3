CRANE_BORDER = TILESIZE / 5 # how much space is left between crane and window
CRANE_MAST_X = 1.5 * TILESIZE # x-origin of mast
CRANE_MAST_THICKNESS = 0.7 # x-scaling of mast
CRANE_BOOM_Y = 1.5 * TILESIZE # how far from top is middle of boom
CRANE_BOOM_LENGTH = MAPX - 1 # x-scaling of boom
CRANE_BOOM_THICKNESS = 0.4 # y-scaling of boom

class Crane
  def initialize()
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png')
  end

  def draw
    @image.draw(CRANE_MAST_X, CRANE_BORDER, ZCRANE,
                scale_x = CRANE_MAST_THICKNESS, scale_y = MAPY - GRASS_THICKNESS,
                color = Gosu::Color::YELLOW) # mast + head
    @image.draw_rot(CRANE_BORDER, CRANE_BOOM_Y, ZCRANE, 0,
                    center_x = 0, center_y = 0.5,
                    scale_x = CRANE_BOOM_LENGTH, scale_y = CRANE_BOOM_THICKNESS,
                    color = Gosu::Color::YELLOW) # boom
  end
end
