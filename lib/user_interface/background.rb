GRASS_THICKNESS = 0.5 # y-scaling of grass
GRASS_Y = WINDOW_HEIGHT - GRASS_THICKNESS * TILESIZE # y-origin of mast

class Background
  def initialize()
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png')
  end

  def draw
    @image.draw(0, 0, ZBACKGROUND,
                scale_x = MAPX, scale_y = MAPY,
                color = Gosu::Color::CYAN) # sky
    @image.draw(0, GRASS_Y, ZBACKGROUND,
                scale_x = MAPX, scale_y = GRASS_THICKNESS,
                color = Gosu::Color::GREEN) # grass
  end
end
