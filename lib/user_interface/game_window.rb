TILESIZE = 50
MAPX = 10
MAPY = MAPX
LINE_HEIGHT = 20

WINDOW_WIDTH = MAPX * TILESIZE
WINDOW_HEIGHT = MAPY * TILESIZE
CENTRE = MAPX * TILESIZE / 2

require_relative './background'
require_relative './infopane'

require_relative './../items/blockyard'
require_relative './../items/crane'
require_relative './../items/hook'

ZBACKGROUND = 0
ZCRANE = 1
ZITEMS = 2
ZTEXT = 3

# Main window
class GameWindow < Gosu::Window
  attr_reader :win

  def initialize(version,
                 width = WINDOW_WIDTH, \
                 height = WINDOW_HEIGHT, \
                 fullscreen = false)
    super(width, height, fullscreen)

    # Set version name
    self.caption = "Crane Construction #{version}"
    $debug = false # debug messages turned off by default
    reset!

    @background = Background.new()
    @infopane = Infopane.new(self)

    @blockyard = Blockyard.new()
    @crane = Crane.new()
    @hook = Hook.new(self, @blockyard, @crane)

    @blockyard.produce!
  end

  # Start processing the pushed button
  def button_up(key)
    case key
    when Gosu::KB_ESCAPE then
      self.close
    when Gosu::KB_BACKSPACE then
      reset
    when Gosu::KB_TAB then
      switch_debug!
    else
      @button = key
    end
  end

  # Process given button
  def update
    @infopane.update

    unless @won
      @blockyard.update(@button)
      @hook.update(@button)
      check_win
    end

    # Stop procsesing this button
    @button = nil
  end

  # Check whether player has won yet
  def check_win
    false # TODO
  end

  # Draw scene
  def draw
    @background.draw
    @infopane.draw

    @blockyard.draw
    @crane.draw
    @hook.draw
  end

  # Reset scene
  def reset
    reset!
    @infopane.reset!
    @blockyard.reset!
    @hook.reset!
  end

  # Load default setup
  def reset!
    @won = false
  end

  # Switch debug flag
  def switch_debug!
    $debug = !$debug
  end
end
